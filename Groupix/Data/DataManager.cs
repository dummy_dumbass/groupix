﻿using Groupix.ObserverRealisation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Groupix.Data
{
    public class DataManager 
    {
        private static readonly DataManager _instance;
        private GroupDbContext _dataBase;
        private JsonReader _jsonReader;
        private string _lastLoadedType = string.Empty;
        private IEnumerable _lastLoadedQuery;
        private FakeConnection _fakeConnection;

        private DataManager()
        {
            _dataBase = new GroupDbContext();
            _jsonReader = new JsonReader();
            _fakeConnection = new FakeConnection();
        }

        static DataManager()
        {
            if (_instance == null) _instance = new DataManager();
        }

        public static DataManager Instance
        {
            get
            {
                return _instance;
            }
        }

        public GroupDbContext DataBase
        {
            get
            {
                return _dataBase;
            }
        }

        public JsonReader LoadData
        {
            get
            {
                return _jsonReader;
            }
        }

        public string LastLoadedType
        {
            get
            {
                return _lastLoadedType;
            }
        }

        public IEnumerable LoadAnyData(string filename)
        {
            _lastLoadedType = _jsonReader.GetJsonObjectType(filename);
            
            switch (_lastLoadedType)
            {
                case "Student":
                    _lastLoadedQuery = _jsonReader.Students(filename);
                    return _jsonReader.Students(filename);
                case "Teacher":
                    _lastLoadedQuery = _jsonReader.Teachers(filename);
                    return _jsonReader.Teachers(filename);
                case "Subject":
                    _lastLoadedQuery = _jsonReader.Subjects(filename);
                    return _jsonReader.Subjects(filename);
                case "Class":
                    _lastLoadedQuery = _jsonReader.Classes(filename);
                    return _jsonReader.Classes(filename);
                case "Mark":
                    _lastLoadedQuery = _jsonReader.Marks(filename);
                    return _jsonReader.Marks(filename);
                default:
                    return null;
            }       
        }

        public int StoreLoadedData()
        {
            int changes;

            switch(_lastLoadedType)
            {
                case "Student":
                    foreach(Student s in _lastLoadedQuery)
                    {
                        _dataBase.Students.Add(s);
                    }
                    changes = _dataBase.SaveChanges();
                    break;
                case "Teacher":
                    foreach (Teacher t in _lastLoadedQuery)
                    {
                        _dataBase.Teachers.Add(t);
                    }
                    changes = _dataBase.SaveChanges();
                    break;
                case "Subject":
                    foreach (Subject s in _lastLoadedQuery)
                    {
                        _dataBase.Subjects.Add(s);
                    }
                    changes = _dataBase.SaveChanges();
                    break;
                case "Class":
                    foreach (Class c in _lastLoadedQuery)
                    {
                        _dataBase.Classes.Add(c);
                    }
                    changes = _dataBase.SaveChanges();
                    break;
                case "Mark":
                    foreach (Mark m in _lastLoadedQuery)
                    {
                        _dataBase.Marks.Add(m);
                    }
                    changes = _dataBase.SaveChanges();
                    break;
                default:
                    changes = -1;
                    break;
            }

            _fakeConnection.StoredData(_lastLoadedType);
            _fakeConnection.UpdateObservers();

            return changes;
        }

        public FakeConnection UiUpdater
        {
            get
            {
                return _fakeConnection;
            }
        }



    }
}
