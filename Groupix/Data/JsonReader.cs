﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Groupix.Data
{
    public class JsonReader
    {
        public JsonReader()
        {

        }

        public IEnumerable<Student> Students(string filename)
        {
            using (StreamReader r = new StreamReader(filename))
            {
                string json = r.ReadToEnd();
                return JsonConvert.DeserializeObject<List<Student>>(json);
            }
        }

        public IEnumerable<Teacher> Teachers(string filename)
        {
            using (StreamReader r = new StreamReader(filename))
            {
                string json = r.ReadToEnd();
                return JsonConvert.DeserializeObject<List<Teacher>>(json);
            }
        }

        public IEnumerable<Subject> Subjects(string filename)
        {
            using (StreamReader r = new StreamReader(filename))
            {
                string json = r.ReadToEnd();
                return JsonConvert.DeserializeObject<List<Subject>>(json);
            }
        }

        public IEnumerable<Class> Classes(string filename)
        {
            using (StreamReader r = new StreamReader(filename))
            {
                string json = r.ReadToEnd();
                return JsonConvert.DeserializeObject<List<Class>>(json);
            }
        }

        public IEnumerable<Mark> Marks(string filename)
        {
            using (StreamReader r = new StreamReader(filename))
            {
                string json = r.ReadToEnd();
                return JsonConvert.DeserializeObject<List<Mark>>(json);
            }
        }

        public string GetJsonObjectType(string filename)
        {
            List<JObject> readedObjects;

            using (StreamReader r = new StreamReader(filename))
            {
                string json = r.ReadToEnd();
                readedObjects = JsonConvert.DeserializeObject<List<JObject>>(json);
            }

            JObject anyObject = readedObjects[0];

            bool hasMailProp = false;

            foreach (JProperty property in anyObject.Properties())
            {

                //Console.WriteLine(property.Name);

                if (property.Name.Contains("Start"))
                {
                    return "Class";
                }

                if (property.Name.Contains("TeacherId"))
                {
                    return "Subject";
                }

                if (property.Name.Contains("ClassId"))
                {
                    return "Mark";
                }

                if (property.Name.Contains("Mail") && !hasMailProp)
                {
                    hasMailProp = true;
                }

                if(hasMailProp && property.Name.Contains("Description"))
                {
                    return "Teacher";
                }
            }

            return "Student"; 
        }

    }
}
