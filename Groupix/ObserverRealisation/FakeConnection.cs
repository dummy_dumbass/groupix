﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Groupix.ObserverRealisation
{
    public class FakeConnection : IObservable
    {
        private List<IObserver> _observers;
        private string _log;

        public FakeConnection()
        {
            _observers = new List<IObserver>();
        }

        public void AddObserver(IObserver o)
        {
            _observers.Add(o);
        }

        public void RemoveObserver(IObserver o)
        {
            _observers.Remove(o);
        }

        public void UpdateObservers()
        {
            foreach(IObserver o in _observers)
            {
                o.Update(_log);
            }
        }

        public void StoredData(string dataType)
        {
            _log = "Таблица " + dataType + " была изменена!";
        }

    }
}
