﻿using Groupix.Data;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Groupix.Tests
{
    [TestFixture]
    class DataManagerTests
    {
        [Test]
        public void LastReadedType()
        {
            DataManager.Instance.LoadAnyData("students.txt");
            Assert.AreEqual("Student", DataManager.Instance.LastLoadedType);
            DataManager.Instance.LoadAnyData("teachers.txt");
            Assert.AreEqual("Teacher", DataManager.Instance.LastLoadedType);
            DataManager.Instance.LoadAnyData("subjects.txt");
            Assert.AreEqual("Subject", DataManager.Instance.LastLoadedType);
            DataManager.Instance.LoadAnyData("classes.txt");
            Assert.AreEqual("Class", DataManager.Instance.LastLoadedType);
            DataManager.Instance.LoadAnyData("marks.txt");
            Assert.AreEqual("Mark", DataManager.Instance.LastLoadedType);
        }
    }
}
