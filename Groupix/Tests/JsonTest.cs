﻿using Groupix.Data;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Groupix.Tests
{
    [TestFixture]
    class JsonTest
    {
        private List<Student> studentsInput = DataManager.Instance.LoadData.Students("students.txt").ToList();
        private List<Teacher> teachersInput = DataManager.Instance.LoadData.Teachers("teachers.txt").ToList();
        private List<Subject> subjectsInput = DataManager.Instance.LoadData.Subjects("subjects.txt").ToList();
        private List<Class> classesInput = DataManager.Instance.LoadData.Classes("classes.txt").ToList();
        private List<Mark> markssInput = DataManager.Instance.LoadData.Marks("marks.txt").ToList();

        [Test]
        public void EntityReadingTest()
        {
            //student
            Student student = studentsInput[0];
            Assert.AreEqual("Вольнов П.А", student.Name);
            Assert.AreEqual("volnov@v.com", student.Mail);
            Assert.AreEqual(21, student.Age);

            //teacher
            Teacher teacher = teachersInput[0];
            Assert.AreEqual("учитель4", teacher.Name);
            Assert.AreEqual("uch4@v.com", teacher.Mail);
            Assert.AreEqual("Учитель ОП", teacher.Description);
            Assert.AreEqual(30, teacher.Age);

            //subject
            Subject subject = subjectsInput[0];
            Assert.AreEqual("ОП", subject.Name);
            Assert.AreEqual("Основы програмирования", subject.Description);
            Assert.AreEqual(4, subject.TeacherId);

            //class
            Class classIn = classesInput[0];
            Assert.AreEqual(new DateTime(2017,12,24,8,30,0), classIn.Start);
            Assert.AreEqual(new DateTime(2017,12,24,10,0,0), classIn.End);
            Assert.AreEqual(4, classIn.SubjectId);

            //mark
            Mark mark = markssInput[0];
            Assert.AreEqual(1,mark.StudentId);
            Assert.AreEqual(4,mark.ClassId);
            Assert.AreEqual(4,mark.Value);
        }

        [Test]
        public void FinalListSizeTest()
        {
            Assert.AreEqual(3,studentsInput.Count);
            Assert.AreEqual(3,teachersInput.Count);
            Assert.AreEqual(3,subjectsInput.Count);
            Assert.AreEqual(6,classesInput.Count);
            Assert.AreEqual(6,markssInput.Count);
        }

        [Test]
        public void GetJsonObjectTypeTest()
        {
            JsonReader jr = new JsonReader();
            Assert.AreEqual("Student",jr.GetJsonObjectType("students.txt"));
            Assert.AreEqual("Teacher", jr.GetJsonObjectType("teachers.txt"));
            Assert.AreEqual("Subject", jr.GetJsonObjectType("subjects.txt"));
            Assert.AreEqual("Class", jr.GetJsonObjectType("classes.txt"));
            Assert.AreEqual("Mark", jr.GetJsonObjectType("marks.txt"));
        }

    }
}
