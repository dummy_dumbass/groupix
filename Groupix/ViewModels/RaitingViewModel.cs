﻿using Groupix.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Groupix.ViewModels
{
    class RaitingViewModel
    {
        private string _studentName;

        private List<int> _raiting;

        public RaitingViewModel(Student st)
        {
            _studentName = st.Name;

            List<Subject> subjects = DataManager.Instance.DataBase.Subjects.ToList();

            _raiting = new List<int>(new int[subjects.Count+1]);

            List<int> initIds = new List<int>();

            foreach(Mark m in st.Marks)
            {
                int subjId = m.Class.SubjectId;

                if (initIds.Contains(subjId))
                {
                    _raiting[subjId] += m.Value;
                }
                else
                {
                    _raiting[subjId] = m.Value;
                    initIds.Add(subjId);
                }
            }
        }

        public List<int> Raiting
        {
            get
            {
                return _raiting;
            }
        }

        public String StudentName
        {
            get
            {
                return _studentName;
            }
        }


    }
}
