﻿using Groupix.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Groupix.ViewModels
{
    class ScheduleViewModel
    {
        private List<DateTime> _subjStartDates;
        private List<DateTime> _subjEndDates;
        private List<String> _time;
        private DateTime _date;

        public ScheduleViewModel(DateTime date)
        {
            _date = date;

            int sbjCount = DataManager.Instance.DataBase.Subjects.Count();

            _subjStartDates = new List<DateTime>(new DateTime[sbjCount + 1]);
            _subjEndDates = new List<DateTime>(new DateTime[sbjCount + 1]);

            foreach (Subject sbj in DataManager.Instance.DataBase.Subjects)
            {
                if(sbj.Classes.Count == 0)
                {
                    _subjStartDates[sbj.SubjectId] = new DateTime(1,1,1);
                    _subjEndDates[sbj.SubjectId] = new DateTime(1, 1, 1);
                    continue;
                }

                Class subjectClass = sbj.Classes.Where( c => c.Start.Day.Equals(date.Day) 
                                                        && c.Start.Month.Equals(date.Month) 
                                                        && c.Start.Year.Equals(date.Year)).FirstOrDefault();

                if (subjectClass != null)
                {
                    _subjStartDates[sbj.SubjectId] = subjectClass.Start;
                    _subjEndDates[sbj.SubjectId] = subjectClass.End;
                }
            }

            _time = new List<string>();



            foreach( DateTime dt in _subjStartDates)
            {
                if (dt.Year != 1)
                {
                    _time.Add(dt.ToString("HH:mm"));
                }
                else
                {
                    _time.Add("-");
                }
            }

            int i = 0;

            foreach (DateTime dt in _subjEndDates)
            {
                if (dt.Year != 1)
                {
                    _time[i] += " - " + dt.ToString("HH:mm");
                }
                i++;
            }
        }

        public List<String> Time
        {
            get
            {
                return _time;
            }
        }

        public String Date
        {
            get
            {
                return _date.ToString(@"MM\/dd\/yyyy");
            }
        }
    }
}
