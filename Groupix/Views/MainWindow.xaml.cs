﻿using Groupix.Data;
using Groupix.ObserverRealisation;
using Groupix.ViewModels;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Groupix.Views
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IObserver
    {

        private List<string> _logs;

        public MainWindow()
        {
            _logs = new List<string>();
            DataManager.Instance.UiUpdater.AddObserver(this);

            InitializeComponent();
            UpdateUI();           
        }

        public void Update(string log)
        {
            _logs.Add(log);
            UpdateUI();
        }

        private void LoadJsonClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            if (openFileDialog.ShowDialog() == true)
            {
                DataManager.Instance.LoadAnyData(openFileDialog.FileName);
                DataManager.Instance.StoreLoadedData();
            }
        }

        private void ResizeRaitingView()
        {
            RaitingView.Columns.Add(new DataGridTextColumn
            {
                // bind to a dictionary property
                Binding = new Binding("StudentName"),
                Header = "Имя студента"
            });

            foreach (Subject sb in DataManager.Instance.DataBase.Subjects)
            {

                RaitingView.Columns.Add(new DataGridTextColumn
                {
                    //bind to a dictionary property
                    Binding = new Binding("Raiting[" + sb.SubjectId + "]"),
                    Header = sb.Name
                });
            }

            foreach (DataGridColumn column in RaitingView.Columns)
            {
                //if you want to size ur column as per the cell content
                column.Width = new DataGridLength(1.0, DataGridLengthUnitType.SizeToCells);
                //if you want to size ur column as per the column header
                column.Width = new DataGridLength(1.0, DataGridLengthUnitType.SizeToHeader);
                //if you want to size ur column as per both header and cell content
                column.Width = new DataGridLength(1.0, DataGridLengthUnitType.Auto);
            }
        }

        private void ResizeScheduleView()
        {
            ScheduleView.Columns.Add(new DataGridTextColumn
            {
                // bind to a dictionary property
                Binding = new Binding("Date"),
                Header = "Дата"
            });

            foreach (Subject sb in DataManager.Instance.DataBase.Subjects)
            {

                ScheduleView.Columns.Add(new DataGridTextColumn
                {
                    //bind to a dictionary property
                    Binding = new Binding("Time[" + sb.SubjectId + "]"),
                    Header = sb.Name
                });
            }

            foreach (DataGridColumn column in ScheduleView.Columns)
            {
                //if you want to size ur column as per the cell content
                column.Width = new DataGridLength(1.0, DataGridLengthUnitType.SizeToCells);
                //if you want to size ur column as per the column header
                column.Width = new DataGridLength(1.0, DataGridLengthUnitType.SizeToHeader);
                //if you want to size ur column as per both header and cell content
                column.Width = new DataGridLength(1.0, DataGridLengthUnitType.Auto);
            }
        }

        private void UpdateRaitingCells()
        {
            List<RaitingViewModel> raitings = new List<RaitingViewModel>();

            foreach (Student st in DataManager.Instance.DataBase.Students)
            {
                raitings.Add(new RaitingViewModel(st));
            }         

            RaitingView.ItemsSource = raitings;
        }

        private void UpdateScheduleCells()
        {
            List<DateTime> dates = new List<DateTime>();
            dates.Add(DateTime.Now.AddDays(-1));
            dates.Add(DateTime.Now);
            //dates.Add(DateTime.Now.AddDays(1));

            List<ScheduleViewModel> schedules = new List<ScheduleViewModel>();

            foreach (DateTime dt in dates)
            {
                schedules.Add(new ScheduleViewModel(dt));
            }

            ScheduleView.ItemsSource = schedules;
        }

        private void UpdateUI()
        {
            RaitingView.Columns.Clear();
            RaitingView.ItemsSource = null;
            ScheduleView.Columns.Clear();
            ScheduleView.ItemsSource = null;
            LogView.ItemsSource = null;

            ResizeRaitingView();
            UpdateRaitingCells();
            ResizeScheduleView();
            UpdateScheduleCells();

            LogView.ItemsSource = _logs;
        }
    }
}
